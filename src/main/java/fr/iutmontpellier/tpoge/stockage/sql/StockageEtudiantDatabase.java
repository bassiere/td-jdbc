package fr.iutmontpellier.tpoge.stockage.sql;

import fr.iutmontpellier.tpoge.metier.entite.Etudiant;
import fr.iutmontpellier.tpoge.metier.entite.Ressource;
import fr.iutmontpellier.tpoge.stockage.Stockage;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class StockageEtudiantDatabase implements Stockage<Etudiant> {
    @Override
    public void create(Etudiant element) {
        SQLUtils utils = SQLUtils.getInstance();
        Connection connection = utils.getConnection();
        String requete = "INSERT INTO EtudiantsOGE (nom, prenom, idRessourceFavorite) VALUES (?, ?, ?)";
        try(PreparedStatement st = connection.prepareStatement(requete)){
            st.setString(1, element.getNom());
            st.setString(2, element.getPrenom());
            st.setInt(3, element.getRessourceFavorite().getIdRessource());

            st.executeUpdate();
        } catch (SQLException e){
            e.printStackTrace();
        }
    }

    @Override
    public void update(Etudiant element) {
        SQLUtils utils = SQLUtils.getInstance();
        Connection connection = utils.getConnection();
        String requete = "UPDATE EtudiantsOGE SET nom = ?, prenom = ?, idRessourceFavorite = ? WHERE idEtudiant = ?";
        try(PreparedStatement st = connection.prepareStatement(requete)){
            st.setString(1, element.getNom());
            st.setString(2, element.getPrenom());
            st.setInt(3, element.getRessourceFavorite().getIdRessource());
            st.setInt(4, element.getIdEtudiant());

            st.executeUpdate();
        } catch (SQLException e){
            e.printStackTrace();
        }
    }

    @Override
    public void deleteById(int id) {
        SQLUtils utils = SQLUtils.getInstance();
        Connection connection = utils.getConnection();
        String requete = "DELETE FROM EtudiantsOGE WHERE idEtudiant = ?";
        try(PreparedStatement st = connection.prepareStatement(requete)){
            st.setInt(1, id);

            st.executeUpdate();
        } catch (SQLException e){
            e.printStackTrace();
        }

    }

    @Override
    public Etudiant getById(int id) {
        Etudiant etu = null;
        SQLUtils utils = SQLUtils.getInstance();
        Connection connection = utils.getConnection();
        String requete = "SELECT * FROM EtudiantsOGE e JOIN RessourcesOGE r ON e.idRessourceFavorite = r.idRessource WHERE idEtudiant = ?";
        try (PreparedStatement st = connection.prepareStatement(requete)){
            st.setInt(1, id);
            try (ResultSet result = st.executeQuery()){
                result.next();
                String nom = result.getString(2);
                String prenom = result.getString(3);
                Ressource r = new Ressource(result.getString(6));
                r.setIdRessource(result.getInt(4));
                etu = new Etudiant(nom, prenom, r);
                etu.setIdEtudiant(id);
            }
        } catch (SQLException e){
            e.printStackTrace();
        }
        return etu;

    }

    @Override
    public List<Etudiant> getAll() {
        List<Etudiant> etudiants = new ArrayList<>();
        SQLUtils utils = SQLUtils.getInstance();
        Connection connection = utils.getConnection();
        String requete = "SELECT * FROM EtudiantsOGE e JOIN RessourcesOGE r ON e.idRessourceFavorite = r.idRessource";
        try (
                PreparedStatement st = connection.prepareStatement(requete);
                ResultSet result = st.executeQuery()
        ){
            while(result.next()){
                int id = result.getInt(1);
                String nom = result.getString(2);
                String prenom = result.getString(3);
                Ressource r = new Ressource(result.getString(6));
                r.setIdRessource(result.getInt(4));
                Etudiant etudiant = new Etudiant(nom, prenom, r);
                etudiant.setIdEtudiant(id);
                etudiants.add(etudiant);
            }
        } catch (SQLException e){
            e.printStackTrace();
        }
        return etudiants;
    }
}
