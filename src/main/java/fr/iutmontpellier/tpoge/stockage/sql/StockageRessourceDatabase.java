package fr.iutmontpellier.tpoge.stockage.sql;

import fr.iutmontpellier.tpoge.metier.entite.Ressource;
import fr.iutmontpellier.tpoge.stockage.Stockage;
import javafx.collections.ObservableArray;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class StockageRessourceDatabase implements Stockage<Ressource> {
    @Override // -create exo 4 2.
    public void create(Ressource element) {
        SQLUtils utils = SQLUtils.getInstance();
        Connection connection = utils.getConnection();
        String requete = "INSERT INTO RessourcesOGE (nom) VALUES (?)";
        try(PreparedStatement st = connection.prepareStatement(requete)){
            st.setString(1, element.getNom());

            st.executeUpdate();
        } catch (SQLException e){
            e.printStackTrace();
        }

    }

    @Override // -update exo 4 2.
    public void update(Ressource element) {
        SQLUtils utils = SQLUtils.getInstance();
        Connection connection = utils.getConnection();
        String requete = "UPDATE RessourcesOGE SET nom = ? WHERE idRessource = ?";
        try(PreparedStatement st = connection.prepareStatement(requete)){
            st.setString(1, element.getNom());
            st.setInt(2, element.getIdRessource());

            st.executeUpdate();
        } catch (SQLException e){
            e.printStackTrace();
        }
    }

    @Override // -create exo 4 2.
    public void deleteById(int id) {
        SQLUtils utils = SQLUtils.getInstance();
        Connection connection = utils.getConnection();
        String requete = "DELETE FROM RessourcesOGE WHERE idRessource = ?";
        try(PreparedStatement st = connection.prepareStatement(requete)){
            st.setInt(1, id);

            st.executeUpdate();
        } catch (SQLException e){
            e.printStackTrace();
        }
    }

    @Override
    public Ressource getById(int id) {
        Ressource ressource = null;
        SQLUtils utils = SQLUtils.getInstance();
        Connection connection = utils.getConnection();
        String requete = "SELECT * FROM RessourcesOGE WHERE idRessource = " + id;
        try(
            Statement st = connection.createStatement();
            ResultSet result = st.executeQuery(requete);
        ){
            result.next();
            int idR = result.getInt(1);
            String nomR = result.getString(2);
            ressource = new Ressource(nomR);
            ressource.setIdRessource(idR);
        } catch ( SQLException e){
            e.printStackTrace();
        }
        return ressource;
    }

    @Override
    public List<Ressource> getAll() {
        List<Ressource> ressources = new ArrayList<>();
        SQLUtils utils = SQLUtils.getInstance();
        Connection connection = utils.getConnection();
        String requete = "SELECT * FROM RessourcesOGE";
        try(
                Statement st = connection.createStatement();
                ResultSet result = st.executeQuery(requete)
        ){
            while(result.next()){
                int idR = result.getInt(1);
                String nomR = result.getString(2);
                Ressource ressource = new Ressource(nomR);
                ressource.setIdRessource(idR);
                ressources.add(ressource);
            }
        } catch ( SQLException e){
            e.printStackTrace();
        }
        return ressources;
    }
}
