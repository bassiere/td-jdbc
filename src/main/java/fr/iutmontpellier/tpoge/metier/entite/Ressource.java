package fr.iutmontpellier.tpoge.metier.entite;

import jakarta.persistence.*;
import org.hibernate.annotations.Collate;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.id.IncrementGenerator;

@Entity
@Table(name="RessourcesOGE")
public class Ressource {

    @Id
    @GenericGenerator(name = "idRessource", type = IncrementGenerator.class)
    @GeneratedValue(generator = "idRessource")
    private int idRessource;

    @Column
    private String nom;

    public Ressource(String nom) {
        this.nom = nom;
    }

    public Ressource() {

    }

    public int getIdRessource() {
        return idRessource;
    }

    public void setIdRessource(int idRessource) {
        this.idRessource = idRessource;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    @Override
    public String toString() {
        return this.nom;
    }
}
